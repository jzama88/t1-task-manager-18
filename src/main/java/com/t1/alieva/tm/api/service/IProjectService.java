package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id) throws AbstractFieldException;

    Project findOneByIndex(Integer index) throws AbstractFieldException;

    Project updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    void remove(Project project) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project removeById(String id) throws AbstractFieldException;

    Project removeByIndex(Integer index) throws IndexIncorrectException, AbstractFieldException;

    Project add(Project project) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project create (String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project create (String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    void clear();

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusById(String id, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

}
