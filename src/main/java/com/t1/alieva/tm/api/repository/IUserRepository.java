package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);


}
