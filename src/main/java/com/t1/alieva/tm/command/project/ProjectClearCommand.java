package com.t1.alieva.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand{

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        getProjectService().clear();
    }
}
